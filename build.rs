#![feature(io)]
#![feature(os)]
#![feature(path)]
use std::os;
use std::old_io::Command;
use std::old_io::fs;
use std::old_io::fs::PathExtensions;

static NANOVG_REPO: &'static str = "http://github.com/memononen/nanovg";

fn main() {
   let lib_path = Path::new(os::getenv("CARGO_MANIFEST_DIR").unwrap()).join("target/native");
   let out_path = Path::new(os::getenv("OUT_DIR").unwrap());

   build_nanovg(&out_path, &lib_path);
   build_nanovg_shim(&out_path, &lib_path);
}

fn build_nanovg(out_path: &Path, lib_path: &Path) {
   let nanovg_path = out_path.join("nanovg");

   if nanovg_path.exists() {
      execute_command(
         "pull nanovg",
         Command::new("git")
            .cwd(&nanovg_path)
            .arg("pull")
      );
   }
   else {
      execute_command(
         "clone nanovg",
         Command::new("git")
            .cwd(out_path)
            .args(&["clone", NANOVG_REPO])
      );
   }

   execute_command(
      "generate nanovg makefile",
      Command::new("premake4")
         .cwd(&nanovg_path)
         .arg("gmake")
   );
   execute_command(
      "build nanovg",
      Command::new("make")
         .env("CFLAGS", "-fPIC")
         .cwd(&nanovg_path.join("build"))
         .args(&["config=release", "verbose=1", "nanovg"])
   );

   fs::copy(&nanovg_path.join("build/libnanovg.a"), &lib_path.join("libnanovg.a")).unwrap();
}

fn build_nanovg_shim(out_path: &Path, lib_path: &Path) {
   let shim_path = Path::new("./shim");

   execute_command(
      "generate nanovg shim makefile",
      Command::new("premake4")
         .cwd(&shim_path)
         .arg(format!("--nanovg-out={}", out_path.join("nanovg/build").display()))
         .arg(format!("--nanovg-root={}", out_path.join("nanovg").display()))
         .arg("gmake")
   );
   execute_command(
      "build nanovg shim",
      Command::new("make")
         .env("CFLAGS", "-fPIC")
         .cwd(&shim_path.join("build"))
         .args(&["config=release", "verbose=1"])
   );

   fs::copy(&shim_path.join("build/libnanovg_shim.a"), &lib_path.join("libnanovg_shim.a")).unwrap();
   println!("cargo:rustc-flags=-L {}", lib_path.display());
}

fn execute_command(desc: &str, command: &Command) {
   match command.output() {
      Ok(out) => println!("{}: {}", desc, String::from_utf8(out.output).unwrap()),
      Err(err) => panic!("Failed to {}: {}", desc, err)
   };
}
