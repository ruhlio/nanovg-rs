#![allow(dead_code)]
#![allow(unreachable_code)]
#![allow(unused_variable)]
#![allow(non_snake_case)]
#![ignore(unstable)]

extern crate libc;

extern crate glutin;
extern crate gl;
extern crate nanovg;

use nanovg::Ctx;

/// evaluate the expression, then check for GL error.
macro_rules! glcheck(
    ($e: expr) => (
        {
            $e;
            assert_eq!(unsafe { gl::GetError() }, 0);
        }
    )
);


mod perf;
mod demo;

fn init_gl() {
    unsafe {
        glcheck!(gl::FrontFace(gl::CCW));
        glcheck!(gl::Enable(gl::DEPTH_TEST));
        glcheck!(gl::Enable(gl::SCISSOR_TEST));
        glcheck!(gl::DepthFunc(gl::LEQUAL));
        glcheck!(gl::FrontFace(gl::CCW));
        glcheck!(gl::Enable(gl::CULL_FACE));
        glcheck!(gl::CullFace(gl::BACK));
    }
}


static mut blowup: bool = false;
static mut screenshot: bool = false;
static mut premult: bool = false;

fn main()
{
	// glfw.window_hint(glfw::ContextVersion(3, 2));
 // 	glfw.window_hint(glfw::OpenglForwardCompat(true));
 // 	glfw.window_hint(glfw::OpenglProfile(glfw::OpenGlCoreProfile));
 // 	glfw.window_hint(glfw::OpenglDebugContext(true));

    let window = match glutin::WindowBuilder::new()
       .with_dimensions(1100, 800)
       .with_title(String::from_str("NanoVG GL3 Rust demo"))
       .with_gl_version((3, 2))
       .build() {
        Ok(window) => window,
        Err(err) => panic!("Failed to create window: {}", err)
       };
    unsafe { window.make_current(); }

    // use glfw to load GL function pointers
    glcheck!(gl::load_with(|name| window.get_proc_address(name)));
    init_gl();

   	let vg: nanovg::Ctx = nanovg::Ctx::create_gl3(nanovg::ANTIALIAS | nanovg::STENCIL_STROKES);
   	//assert!(!vg.ptr.is_null());

    let data = demo::DemoData::load(&vg, "../../res");

//    return test_linebreaks(vg);

	// let mut prevt = glfw.get_time();

	let mut fps = perf::PerfGraph::init(perf::Style::FPS, "Frame Time");

    let mut mx: i32 = 0;
    let mut my: i32 = 0;

    while !window.is_closed()
    {
    	// let t: f64 = glfw.get_time();
        let t: f64 = 0.0;
    	// let dt: f64 = t - prevt;
    	// prevt = t;
    	// fps.update(dt);

        let (winWidth, winHeight) = window.get_outer_size().expect("Couldn't get outer window size");
        let (fbWidth, fbHeight) = window.get_inner_size().expect("Couldn't get inner window size");
        // Calculate pixel ration for hi-dpi devices.
        let pxRatio: f32 = fbWidth as f32 / winWidth as f32;

        // Update and render
        unsafe {
            glcheck!(gl::Viewport(0, 0, fbWidth as i32, fbHeight as i32));

            if premult {
                glcheck!(gl::ClearColor(0.0, 0.0, 0.0, 0.0));
            } else {
                glcheck!(gl::ClearColor(0.3, 0.3, 0.32, 1.0));
            }

            glcheck!(gl::Clear(gl::COLOR_BUFFER_BIT|gl::DEPTH_BUFFER_BIT|gl::STENCIL_BUFFER_BIT));

            glcheck!(gl::Enable(gl::BLEND));
            glcheck!(gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA));
            glcheck!(gl::Enable(gl::CULL_FACE));
            glcheck!(gl::Disable(gl::DEPTH_TEST));
        }

        vg.begin_frame(winWidth as i32, winHeight as i32, pxRatio as f32);

        unsafe { demo::render_demo(&vg, mx as f32, my as f32, winWidth as f32,winHeight as f32, t as f32, blowup, &data); }
        fps.render(&vg, 5.0, 5.0);

        vg.end_frame();

        unsafe {
            gl::Enable(gl::DEPTH_TEST);

	        if screenshot {
	        	screenshot = false;
	        	demo::save_screenshot(fbWidth as u32, fbHeight as u32, premult, "dump.png");
	        }
        }

    	window.swap_buffers();

        for event in window.poll_events() {
            match event {
                glutin::Event::KeyboardInput(glutin::ElementState::Pressed, _, keyCode) => match keyCode {
//                    Some(glutin::VirtualKeyCode::Escape) => window.set_should_close(true),
                    Some(glutin::VirtualKeyCode::Space) => unsafe {blowup = !blowup},
                    Some(glutin::VirtualKeyCode::S) => unsafe {screenshot = true},
                    Some(glutin::VirtualKeyCode::P) => unsafe {premult = !premult},
                    _ => {}
                },
                glutin::Event::MouseMoved((x, y)) => {
                    mx = x;
                    my = y;
                }
                _ => {}
            };
        }
    }
}


//// think linebreaks api needs some love
//fn test_linebreaks(vg:Ctx) {
//    let x=0.0; let y=0.0;
//    let width = 120.0;
//
//    let text = "This is longer chunk of text.\n  \n  Would have used lorem ipsum but she    was busy jumping over the lazy dog with the fox and all the men who came to the aid of the party.";
//    //let text = "01234 6789 1234 6789 1234 6789 123456789012345678901234567890123456789012345678901234567890123456789";
//
//
//    // The text break API can be used to fill a large buffer of rows,
//    // or to iterate over the text just few lines (or just one) at a time.
//    // The "next" variable of the last returned item tells where to continue.
//    let mut start: uint = 0;    // byte pos in utf8 'text' str
//    let end: uint = text.len(); // exclusive
//    'chunks: loop {
//        let text = text.slice(start, end);
//
//println!("{}", text);
//
//        let rows = vg.text_break_lines(text, width, 3);
//        let nrows = rows.len();
//        if nrows == 0 { break 'chunks; }
//        for i in range(0, nrows) {
//            let row = &rows[i];
//            let line = text.slice(row.start_index(), row.end_index());
//
//println!("i: {}  st: {}, en: {}  \t {} \tnext: {}", i, row.start_index(), row.end_index(), line, row.next_index());
//
//        }
//        // Keep going...
//        start += rows[nrows-1].next_index();
//    }
//}
